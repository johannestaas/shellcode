import re
import subprocess

RE_START = re.compile(r'\d+ <\w+>:')
RE_BYTES = re.compile(r'\s*[0-9a-fA-F]+:\s+([^\t]+).*')


def extract_bytes(text):
    lines = text.splitlines()
    for i, line in enumerate(lines):
        if RE_START.match(line):
            break
    else:
        raise ValueError(f'couldnt find start of disassembly:\n{text}')
    lines = lines[i+1:]
    byte_digits = []
    for line in lines:
        match = RE_BYTES.match(line)
        if match is None:
            continue
        byte_digits.extend(match.group(1).strip().split())
    byte_string = ''.join(byte_digits)
    return bytearray.fromhex(byte_string)


def run_and_dump(inp, out):
    text = subprocess.check_output(['objdump', '-D', inp]).decode('utf8')
    byte_string = extract_bytes(text)
    print(f'got: {byte_string!r}')
    with open(out, 'wb') as f:
        f.write(byte_string)
    print(f'dumped to {out}')
    for i, b in enumerate(byte_string):
        if b == 0:
            print(f'detected null-byte at offset {i}')


def main():
    import argparse
    parser = argparse.ArgumentParser()
    parser.add_argument('input_file')
    parser.add_argument('--output', '-o', default=None)
    args = parser.parse_args()
    output_path = args.output or f'{args.input_file}.bin'
    run_and_dump(args.input_file, output_path)


if __name__ == '__main__':
    main()
