#!/usr/bin/env python3
from ast import literal_eval


def mov(reg, value):
    sub = hex(0x7fffffff - value)
    return f'''mov {reg}, 0x7fffffff
sub {reg}, {sub}
'''


def main():
    import argparse
    parser = argparse.ArgumentParser()
    parser.add_argument(
        '--register', '-r', default='rax',
        help='register to load value into, default: %(default)s',
    )
    parser.add_argument(
        'value', type=literal_eval,
        help='value to load into register in python syntax (eg. 10, 0xa, 0b1)',
    )
    args = parser.parse_args()
    print(mov(args.register, args.value))


if __name__ == '__main__':
    main()
