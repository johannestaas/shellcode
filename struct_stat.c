// Need this to get rid of non-POSIX warnings with syscall usage.
#define _GNU_SOURCE

#include <sys/stat.h>
#include <sys/syscall.h>
#include <stdio.h>
#include <unistd.h>

#define SC_STAT 4


void dump_stat(struct stat *st) {
    printf("off_t size: %ld\n", sizeof(st->st_size));
    printf("filesize: %ld\n", st->st_size);
    int offset = (void*)&(st->st_size) - (void*)st;
    printf("offset of st_size field: %d\n", offset);
}


int main() {
    struct stat st;
    const char path[] = "/etc/passwd";
    printf("sizeof struct stat: %ld\n", sizeof(st));
    if (syscall(SC_STAT, path, &st) != 0) {
        fprintf(stderr, "failed to STAT %s\n", path);
        return 1;
    }
    dump_stat(&st);
}
