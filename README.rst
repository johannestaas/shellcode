shellcode
=========

This is a bit of work and experimentation I'm doing with writing custom
shellcode in 64-bit assembly for linux.

Honestly, I think shellcode.s is a modified version I found from something
online, but I can't remember the damn source. Either way it's heavily modified
and pretty simple, so I'm just going to skip giving credit to whoever did it.

author: Johan Nestaas

Compilation
===========

For compiling this into 64-bit linux programs, use this workflow::

   # Use netwide assembler to generate object file.
   nasm -f elf64 shellcode.s -o shellcode.o
   # Use the linker to output the actual runnable binary.
   ld shellcode.o -o shellcode

For getting the actual shellcode bytes to pass into an exploit, or whatever::

   objdump -d shellcode
   # or if you want more familiar intel syntax
   objdump -d -M intel shellcode

You might see stuff like this::

   40100c:       40 b7 02                mov    $0x2,%dil
   40100f:       40 b6 01                mov    $0x1,%sil
   401012:       b0 29                   mov    $0x29,%al
   401014:       0f 05                   syscall         

Then take those middle bytes and output something like this::

   printf '\x40\xb7\x02\x40\xb6\x01\xb0\x29\x0f\x05' > shellcode.bin

I'll include a python script to handle this automatically, but by doing that
you have a raw data file with the bytes you'll want to shove into your exploit.

   
Tools
=====

disasm_to_file.py
-----------------

Takes a compiled binary, then rips out the hex values of the instructions and
dumps a .bin file with them. Also, it warns you of null-bytes.

Example::

   python3 disasm_to_file.py shellcode

mov_nonull.py
-------------

Takes an integer and generates instructions that subtract it from 0x7fffffff
to ensure you can move a null-byte free value into a register.

Example::

   python3 mov_nonull.py 0x333
   python3 mov_nonull.py --register rdx 0x333
   python3 mov_nonull.py 16
   python3 mov_nonull.py 0b01001

sockaddr_to_stack.py
--------------------

Takes an IPv4 address and port, and generates instructions that will
craft a sockaddr_in struct and push it to the stack, without null bytes.

Example::

   $ python3 sockaddr_to_stack.py 127.0.0.1 80 -c
   xor rax, rax
   push rax  ; pushes sin_zero null bytes to stack
   mov rax, 0xffffffff
   sub rax, 0xfeffff80
   shl rax, 32  ; shifts IPv4 address to left side
   add rax, 0xffffffff
   sub rax, 0xaffffffd
   push rax  ; adds in the AF_INET of 2 and port value


Contents
========

shellcode.s
-----------

- null-byte free
- modifies stack

execve of /bin/sh (more accurately, `/bin//sh` to avoid null-bytes).

shellcode_nopush.s
------------------

- null-byte free
- doesn't modify stack, but expects path to program to exist in stack

execve of /bin/sh, doesn't modify stack but expects rsp+0x20 to contain
`/bin//sh\\0` or whatever is to be executed. This is useful if you are loading
your shellcode into the stack and can also get the path to the executable
inside there.

shellcode_static_str.s
----------------------

- null-byte free
- doesn't modify stack, but takes absolute address in memory to program path
- not really portable or useful

This was just some testing I was doing where I had the address of my /bin/sh
string and wanted to just test it directly.

socket.c
--------

This is a C implementation of using syscalls to make a basic TCP/IP client.
It hits 127.0.0.1:80 with a message 'hi buddy'.

To test::

   # Leave this running in one window.
   sudo nc -l -p 80

   # Run this in another window.
   gcc socket.c -o socket_c
   ./socket_c

socket.s
--------

- null-byte free
- modifies stack for crafting a sockaddr_in struct for CONNECT
- somewhat portable but you'd definitely want to modify it for real world usage
- has extraneous code for debugging, with useful exit statuses

This is the ASM version of what the C version above does. It will connect to
localhost on port 80, and just send the message 'hi buddy'.

It has useful exit statuses for debugging, but those could be removed if this
was modified for an exploit.

To test, you'll want to run `sudo nc -l -p 80` in another window to get the
message.

socket_noexit.s
---------------

- null-byte free
- modifies stack for crafting a sockaddr_in struct for CONNECT
- somewhat portable but you'd definitely want to modify it for real world usage
- like the above, but no exit (so it WILL segfault after sending the message)
- no error handling at all

This is another ASM socket client that sends the message 'hi buddy'.
It has no error handling, and is much shorter in length than the above.

To test, you'll want to run `sudo nc -l -p 80` in another window to get the
message.

struct_stat.c
-------------

This was a test to see the results from the stat syscall, find the filesize of
a file, and see what offset it is from the start of the buffer.
This will be necessary for code to get the filesize of /etc/passwd and send
it over a socket.

The output I saw::

   sizeof struct stat: 144
   off_t size: 8
   filesize: ... size in bytes ...
   offset of st_size field: 48

send_etc_passwd.s
-----------------

- null-byte free
- modifies stack (socket connection struct, reading from /etc/passwd, etc)
- somewhat portable, but you would want to modify the destination IP and port
- no exit, ends up segfaulting at end even on success
- no error handling at all

This combines a lot of work from the socket code and has added functionality to
open /etc/passwd, read it into the stack, then send that over the wire using
the same socket code.
