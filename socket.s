; Constants
AF_INET equ 2
SOCK_STREAM equ 1
PROTOCOL equ 0

; System Calls
SC_READ equ 0
SC_WRITE equ 1
SC_SOCKET equ 41
SC_CONNECT equ 42
SC_SENDTO equ 44
SC_EXIT equ 60


section .text
  global _start
    _start:
      ; clear arguments
      xor rax, rax
      xor rdi, rdi
      xor rsi, rsi
      xor rdx, rdx

      ; invokes socket(AF_INET, SOCK_STREAM, 0)
      ; https://filippo.io/linux-syscall-table/
      mov dil, AF_INET      ; dil is low 8 bits of rdi
      mov sil, SOCK_STREAM  ; sil is low 8 bits of rsi
      ; mov dl, PROTOCOL    ; It was cleared already with xor, so just for notes and avoiding null bytes.
      mov al, SC_SOCKET     ; low 8 bits of rax
      syscall               ; create socket

      ; Test for zero return value.
      test rax, rax        ; Test, to see if it's negative.
      js _exit_1           ; Exit status 1 if the sign flag is set (-1 is error).

      ; connect to server
      ; int connect(int sockfd, const struct sockaddr *addr, socklen_t addrlen);
     	;
			; Needs this struct 
			; struct sockaddr_in {

			;     short   sin_family;         // AF_INET
			;     u_short sin_port;           // need htons(port_number) for this, so for port 80 it'll be 0x5000 (little-endian 16-bit)
			;     struct  in_addr sin_addr;   // 64-bit unsigned long s_addr
			;     char    sin_zero[8];        // Padding to make this 16 bytes. Just push xor'd rax
			; }; 
			; And this one
      ; struct in_addr {
      ;         unsigned long s_addr;   // 64-bits
      ; };

      mov rdi, rax                  ; get the socket file descriptor as first arg.
      xor rax, rax                  ; push the sin_zero 8 byte padding
      push rax                      ; sin_zero (all zeroes for padding)
      mov rbx, 0xffffffff           ; math to load 127.0.0.1 and avoid null bytes
      sub rbx, 0xfeffff80           ; looks like 0x0100007f
      shl rbx, 32                   ; shift to left side of the 32 bits of 64-bit register.
      add rbx, 0x7fffffff           ; math to load 0x50000002 into the right side
      sub rbx, 0x2ffffffd           ; this is pushing AF_INET short (0x02) and short port 80 (0x50)
      push rbx
      ; arg1 is FD, in rdi
      mov rsi, rsp          ; arg 2 is the address of the sockaddr_in, the stack pointer with 16 bytes
      mov rdx, 0x7fffffff
      sub rdx, 0x7fffffef           ; hacks in sizeof(sockaddr_in), or 16
      ; already XORd above
      mov al, SC_CONNECT
      syscall               ; connects to 127.0.0.1:80

      test rax, rax         ; zero is success, -1 is error.
      js _exit_2            ; jump if it's signed, and -1 is an error result.

      ; ssize_t sendto(int sockfd, const void *buf, size_t len, int flags, const struct sockaddr *dest_addr, socklen_t addrlen);
      ; rdi should still be the socket file descriptor
      mov rbx, 'hi buddy'   ; Our lovely message to the server
      push rbx              ; put it on the stack
      mov rsi, rsp          ; 2nd arg is the stack pointer with "hi buddy"
      mov rdx, 0x7fffffff   ; hack in the value 8, for the length of the message "hi buddy"
      sub rdx, 0x7ffffff7   ; no null bytes
      xor rcx, rcx          ; 0 for flags
      xor r8, r8            ; NULL for dest_addr
      xor r9, r9            ; 0 for addrlen
      xor rax, rax
      mov al, SC_SENDTO
      syscall               ; writes "hi buddy" to the socket
      
      test rax, rax         ; returns number of written bytes, and -1 is error.
      js _exit_3            ; jump if it's signed, because -1 is an error result.

    ; Unnecessary, but somewhat nice to exit cleanly instead of segfault, and it's good to have some
    ; error codes for debugging... using inc to avoid null bytes.
    _exit_0:
      xor rax, rax
      mov al, SC_EXIT
      xor rdi, rdi          ; status 0
      syscall               ; exit

    _exit_1:
      xor rax, rax
      mov al, SC_EXIT
      xor rdi, rdi
      inc rdi               ; status 1
      syscall               ; exit

    _exit_2:
      xor rax, rax
      mov al, SC_EXIT
      xor rdi, rdi
      inc rdi
      inc rdi               ; status 2
      syscall               ; exit

    _exit_3:
      xor rax, rax
      mov al, SC_EXIT
      xor rdi, rdi
      inc rdi
      inc rdi
      inc rdi               ; status 3
      syscall               ; exit

    ; This was just for debugging, to exit with the status of the syscall result.
    _exit_rax:
      mov rdi, rax
      xor rax, rax
      mov al, SC_EXIT
      syscall               ; exit
