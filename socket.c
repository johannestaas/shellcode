// Need this to get rid of non-POSIX warnings with syscall usage.
#define _GNU_SOURCE

#include <stdio.h>
#include <sys/syscall.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <unistd.h>

#define SC_SOCKET 41
#define SC_CONNECT 42
#define SC_SENDTO 44

/*
 * These are from the includes, so they're unnecessary to have.
 * ... but, for reference:
 * #define AF_INET 2
 * #define SOCK_STREAM 1
 */


/*
 * This just dumps out the sizeof fields in the sockaddr_in and the hex values
 * of each byte in the entire loaded struct.
 */
void dump_saddr(struct sockaddr_in *saddr) {
    printf("Size of saddr: %ld\n", sizeof(*saddr));
    printf("Size of saddr.in_addr: %ld\n", sizeof(saddr->sin_addr));
    printf("Dump saddr in memory:\n");
    char *start = (char*)saddr;
    char *end = start + sizeof(*saddr);
    int i = 0;
    for (char *c=start; c<end; c++)
        printf("  byte %02d: %02x\n", i++, (unsigned char)*c);
}


int main() {
    // SOCKET
    int sock = syscall(SC_SOCKET, AF_INET, SOCK_STREAM, 0);
    if (sock <= 0) {
        fprintf(stderr, "Failed SOCKET (%d)\n", sock);
        return 1;
    } else {
        printf("Successful SOCKET syscall\n");
        printf("Got socket file descriptor: %d\n", sock);
    }

    // CONNECT
    struct sockaddr_in saddr;
    saddr.sin_family = AF_INET;
    // Set sin_addr to localhost.
    // Should look like 0x0100007f
    // inet_aton should work too, but pton version can take IPv6.
    inet_pton(AF_INET, "127.0.0.1", &(saddr.sin_addr));
    // Set port to 80. This gets turned into a 16-bit short in little endian.
    // Should look like 0x5000
    saddr.sin_port = htons(80);
    // Just debugging and showing in memory byte order.
    dump_saddr(&saddr);
    if (syscall(SC_CONNECT, sock, &saddr, sizeof(saddr)) != 0) {
        fprintf(stderr, "Failed CONNECT\n");
        return 2;
    } else {
        printf("Successful CONNECT syscall\n");
    }

    // SENDTO
    char buf[] = "hi buddy";
    int bytes_written = syscall(SC_SENDTO, sock, buf, 8, 0, NULL, 0);
    if (bytes_written != 8) {
        fprintf(stderr, "Failed SENDTO (%d)\n", bytes_written);
        return 3;
    } else {
        printf("Successful SENDTO syscall\n");
        printf("Bytes written: %d\n", bytes_written);
    }
}
